const { RESTDataSource } = require('apollo-datasource-rest');

class StationAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://api.test.virtaglobal.com/';
  }

  async getStations({ station_ID }) {
    let data;
    if (station_ID !== undefined) {
      data = await this.get('stations/' + station_ID);
      return [data];
    }
    data = await this.get(`stations/`);
    return data;
  }
  
}

module.exports = StationAPI;
