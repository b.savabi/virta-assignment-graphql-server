const { ApolloServer } = require("apollo-server");

const typeDefs = require("./types");
const resolvers = require('./resolvers')
const StationAPI = require("./data-source");

const server = new ApolloServer({
	typeDefs,
	resolvers,
	dataSources: () => ({
    stationAPI: new StationAPI()
  }),
	introspection: true, // To enable playground on production 
  playground: true, // To enable playground on production 
});

server.listen({
	port: process.env.PORT || 4000
}).then(({ url }) => {
	console.log(`🚀 Server ready at ${url}`);
});
