module.exports = {
  Query: {
    station: (parent, { station_ID }, { dataSources }) =>
      dataSources.stationAPI.getStations({ station_ID })
  }
}
