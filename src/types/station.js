const { gql } = require("apollo-server");

module.exports = [
  gql`
    type Station {
      station_ID: Int!
      custom_evse_id: String
      location_ID: Int
      seller_ID: Int
      name: String
      connected: Int
      position: String
      available: Int
      lastconnect: String
      roaming_identifier_cpo: String
    }
    type Query {
      station(station_ID: Int): [Station]
    }
  `
];
