# Virta assignment - GraphQL server

__UI: (Deployed on Google Firebase)__

[https://virta-assignment-d22a2.web.app/](https://virta-assignment-d22a2.web.app/)


__GraphQL server endpoint: (Running on Heroku)__

[https://virta-assignment.herokuapp.com/](https://virta-assignment.herokuapp.com/)


## Changelog

### v2.0.0
- Fetch data from the REST API.

### v1.0.0
- Initial version working with mock data.